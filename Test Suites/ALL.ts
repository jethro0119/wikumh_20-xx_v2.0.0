<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ALL</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>592908ab-46f3-4381-b02d-d5caf9ca0f9f</testSuiteGuid>
   <testCaseLink>
      <guid>76056b3c-c6bc-4c66-95be-83ae6e0d4d2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC1_Connect_login_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1bcbe9b2-73b9-4cbe-bc90-5bd0fbd23c60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC2_Login_success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3dda52eb-6714-41fc-bbaf-e37aa205302c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC3_Login_Fail_wrong_name</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c1637bd-e72e-49f2-8003-b9d5914f465f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC4_Login_Fail_wrong_password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>929beea2-723c-4d25-a5f9-e923fa611b83</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/System_Manage/Page_Check/TC10_Tag_Page_Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7e34f49d-2df2-4400-8cb0-38ecd464ad6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/System_Manage/Page_Check/TC11_Vaccine_Page_Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2026bdf4-9417-4a1a-b650-4e1bde60c335</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/System_Manage/Page_Check/TC12_GIS_Page_Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59c93d29-f2ff-4071-b78b-5b8366393b14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/System_Manage/Page_Check/TC13_House_Info_Page_Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8feb4ba8-11a0-4674-a72a-e2c6caad39e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/System_Manage/Page_Check/TC14_Vaccination_Page_Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7926c79-d209-4142-9ca7-7ed5e2865d64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/System_Manage/Page_Check/TC15_Analysis_Page_Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e26fed1a-5091-4355-ae63-0e34995036a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/System_Manage/Page_Check/TC17_CaseOverview_Page_Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc5d9e15-26c2-485b-a270-6ed81c8b6912</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/System_Manage/Page_Check/TC18_GIS_Search_Page_Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>849aa0f2-38d2-4889-a659-13fc0b5ea603</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/System_Manage/Page_Check/TC19_Insurance_Upload_Page_Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f758fff3-30c1-4330-a65c-2dd8830c3648</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/System_Manage/Page_Check/TC20_Schedule_Plan_Page_Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f877483c-81ee-4ac5-b681-95ada8eaf60a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/System_Manage/Page_Check/TC8_IOT_Page_Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0bc046b-b178-4b01-8392-76445a0c55f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/System_Manage/Page_Check/TC9_Schedule_Page_Check</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
