import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://kmuh.cctrialpack.com/#/')

WebUI.delay(2)

WebUI.click(findTestObject('Login_Page/Page_Login/Login_div_account_type'))

WebUI.delay(1)

WebUI.click(findTestObject('Login_Page/Page_Login/Login_li_SystemAccount'))

WebUI.delay(1)

WebUI.sendKeys(findTestObject('Login_Page/Page_Login/Login_input_account'), 'hc_admin_1_6403800@yopmail.co')

WebUI.sendKeys(findTestObject('Login_Page/Page_Login/Login_input_password'), '123456789')

WebUI.click(findTestObject('Login_Page/Page_Login/Login_button_'))

WebUI.delay(2)

a = WebUI.getText(findTestObject('Login_Page/Page_Login/div_Login_Alert'))

assert a.contains('帳號或密碼錯誤')

WebUI.delay(2)

WebUI.closeBrowser()

